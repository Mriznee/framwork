<?php

namespace Dev\Interfaces;

interface ControllerInterface 
{
    // public function __construct();

    public function index();

    public function create();

    public function store($id,$request,$params);

    public function edit();

    public function update($id,$request,$params);

    public function delete($id,$request,$params); 

}