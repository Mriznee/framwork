<?php

namespace Dev\System;

class ServiceContainer 
{
    private static $instance;
    private static $bindings = [];
    private static $instances =[];

    private function __construct(){} 
    private function __clone(){}
    

    public static function getInstance()
    {
        if (!isset(self::$instance)) 
        {
            self::$instance = new ServiceContainer();
        }
        return self::$instance;        
    }

    public static function singlton($key , $value)
    {
        if (!isset(self::$instances[$key])) 
        {
            self::$instances[$key] = new $value();
        }
        return self::$instances;        
    }

    public function bind($key, $value)
    {
        $this->bindings[$key] = $value;

        return $this;
    }

    public function make($key)
    {
        if (array_key_exists($key, $this->instances)) {
            return $this->instances[$key];
        }
    
        if (array_key_exists($key, $this->bindings)) {
            $resolver = $this->bindings[$key];
            return $resolver();
        }
        throw new \Exception('Unable to resolve binding from container.');
    }

    public static function singltonWith($key , $value, $param)
    {
        if (!isset(self::$instances[$key])) 
        {
            self::$instances[$key] = new $value($param);
        }
        return self::$instances;        
    }

}