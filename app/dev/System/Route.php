<?php

namespace Dev\System;

use Dev\Traits\Container;
use Dev\Traits\Debug;
use Dev\Helpers\ErrorHandler;


class Route
{
    use Debug;
    use Container;
    
    public $get     = Array();
    public $post    = Array();
    public $put     = Array();
    public $delete  = Array();
    public $url     = Array();
    public $method  = null;
    public $routeList   = Array();
    public $sendUrl     = Array();
    public $redirect    = Array();


    
    public function __construct()
    {
        $this->loadRoute();
        $this->getUrl();
        $this->getMethod();
    }

    public function loadRoute()
    {
        $this->routeList = require_once __DIR__ . ('/../Config/Route.php');       

    }

    public function getUrl()
    {
        if(isset($_SERVER['REQUEST_URI']))
        {
            $this->url = explode('?',$_SERVER['REQUEST_URI'],2);
        }
        else
        {
            ErrorHandler::errorMessage('Could find the Request 101-URL ');  
        }
    }
    
    public function getMethod()
    {
        if(isset($_SERVER['REQUEST_METHOD']))
        {
            $this->method = explode('?',$_SERVER['REQUEST_METHOD']);
        }
        else 
        {
            ErrorHandler::errorMessage('Could find the Request 102-REQUEST METHOD');
        }
    }   

    public  function get($expression, $name, $controller,$method = NULL)
    {
       array_push($this->get, Array(
           'expression'     =>  $this->breakExpression($expression),
            'name'          =>  $name,
            'controller'    =>  $controller,
            'method'        =>  $method,
       ));
    }

    public  function post($expression, $name, $controller, $method = NULL)
    {
        array_push($this->post,Array(
            'expression'     =>  $this->breakExpression($expression),
            'name'          =>  $name,
            'controller'    =>  $controller,
            'method'        =>  $method,
       ));
    }

    public  function put($expression, $name, $controller, $method = NULL)
    {
        array_push($this->put,Array(
            'expression'     =>  $this->breakExpression($expression),
            'name'          =>  $name,
            'controller'    =>  $controller,
            'method'        =>  $method,
       ));
    }

    public  function delete($expression, $name, $controller,$method = NULL)
    {
        array_push($this->delete,Array(
            'expression'     =>  $this->breakExpression($expression),
            'name'          =>  $name,
            'controller'    =>  $controller,
            'method'        =>  $method,
       ));
    }  

    public function resource( $expression, $name, $controller)
    {
        $this->get($expression, $name, $controller, 'index');
        $this->post($expression, $name, $controller,'store');
        $this->put($expression, $name, $controller, 'update');
        $this->delete($expression, $name, $controller,'dele');
    }

    public function redirect($expression)
    {
        array_push($this->redirect,Array(
            'expression'     =>  $expression,
       ));
    }

    private function breakExpression($expression)
    {
        $y = null;
        $newExpression = Array();
        $method = (explode('/',$expression));
        $y = count($method);       
        for($z = 2; $z == $y; $z++)
        {
            if(isset($method[$z]) && !empty($method[$z]))
            {   
                array_push ($newExpression, Array(
                    'resourcename' => $method[$z], 
                    'resourceid'   => (ltrim(rtrim($method[$z+1],'}'),'{')),
                    ));
   
                    $z=$z+1;
            }
            
        }
       return $newExpression;
    }
    
    public function search_route($url, $routlist) {

        foreach ($routlist as $route) {
            if ($route['url'] == $url) {
                // var_dump( $route['url']);
                return $route;
            }
        }
        return  $this->view(404);
    }
        
}