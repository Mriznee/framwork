<?php

namespace Dev\Traits;

Trait Container
{
    protected $service  = [];

    protected function __clone(){}
 
    protected function __construct(){}

    public static function getInstance($name)
    {
        if (!isset(self::$service)) 
        {
            self::$service = new $name();
        }
        return self::$service;
        
    }
    public function bind($key, $value)
    {
        $this->bindings[$key] = $value;

        return $this;
    }

    public function make($key)
    {
        if (array_key_exists($key, $this->services)) {
            return $this->services[$key];
        }
    
        if (array_key_exists($key, $this->bindings)) {
            $resolver = $this->bindings[$key];
            return $resolver();
        }
    
        throw new \Exception('Unable to resolve binding from container.');
    }

    public static function singltonWith($key , $value, $param)
    {
        if (!isset(self::$services[$key])) 
        {
            self::$services[$key] = new $value($param);
        }
        return self::$services;
        
    } 
  
}