<?php

namespace Dev\Traits;

Trait Debug
{  

    public static function dump()
    {
        $args = func_get_args();

        echo "\n<pre style=\"border:1px solid #ccc;padding:10px;" .
            "margin:10px;font:14px courier;background:whitesmoke;" .
            "display:block;border-radius:4px;\">\n";

        $trace = debug_backtrace(false);
        $offset = (@$trace[2]['function'] === 'dump_d') ? 2 : 0;

        echo "<span style=\"color:red\">" .
            @$trace[1+$offset]['class'] . "</span>:" .
            "<span style=\"color:blue;\">" .
            @$trace[1+$offset]['function'] . "</span>:" .
            @$trace[0+$offset]['line'] . " " .
            "<span style=\"color:green;\">" .
            @$trace[0+$offset]['file'] . "</span>\n";

        if ( ! empty($args)) 
        {
            call_user_func_array('print_r', $args);
        }
        echo "</pre>\n";
    }



    public function debug()
    {
        error_reporting(E_ALL);
        $args = func_get_args();
        $backtrace = debug_backtrace();
        $file = file($backtrace[0]['file']);
        $src  = $file[$backtrace[0]['line']-1];  // select debug($varname) line where it has been called
        $pat  = '#(.*)'.__FUNCTION__.' *?\( *?\$(.*) *?\)(.*)#i';  // search pattern for wtf(parameter)
        $arguments  = trim(preg_replace($pat, '$2', $src));  // extract arguments pattern
        $args_arr = array_map('trim', explode(',', $arguments));  
        
        print '<pre><style>
            .warning {
                border: 2px ridge #000000;
                background-color: #eaeaea;
                padding: .5rem;
                display: flex;
                flex-direction: column;
            }     
            .warning p {
                font: small-caps bold 1.2rem sans-serif;
                text-align: center;
            }
            
            .dev {
                border: 2px ridge #ff3333;
                background-color: #eaeaea;
                padding: .5rem;
                display: flex;
                flex-direction: column;
           
            } 
            .debug {
                border: 2px ridge #000000;
                background-color: #eaeaea;
                padding: .5rem;
                display: flex;
                flex-direction: column;
           
            }   
            .vdebug {
                border: 2px ridge #b5b2ff;
                background-color: #eaeaea;
                padding: .5rem;
                display: flex;
                flex-direction: column;
           
            }
            .xdebug {
                border: 2px ridge #00b21e;
                background-color: #eaeaea;
                padding: .5rem;
                display: flex;
                flex-direction: column;
           
            }           

          </style>'.PHP_EOL;

        print '<div class="warning">'.PHP_EOL;
        foreach ($args as $key => $arg) {

            print '<div><div class="dev">';

            array_walk(debug_backtrace(), function($a,$b) use($args){ 
                print_r ('<br>'.$a['function'] . '<br> '. $a['function'] .'<br> basename ='.$a['file'].': '.$a['line']);
                // print_r ($b['function'] . '<br> '. $b['function'] .'<br> basename ='.$b['file'].': '.$b['line']);
            
            
            });
          
            print '</div>'.PHP_EOL;
            
            if (is_array($arg)) {

                print '<div class="vdebug">  <b> Variables   -----> '.$args_arr[$key].' = </b>';

                print_r(htmlspecialchars(print_r($arg)), ENT_COMPAT);

                print '</div>'.PHP_EOL;

            } 
            elseif (is_object($arg)) 
            {
                print '<div class="xdebug"><pre>'.$args_arr[$key].' = ';

                print_r(htmlspecialchars(print_r(var_dump($arg))), ENT_COMPAT, 'UTF-8');

                print '</pre></div>'.PHP_EOL;

            } 
            else 
            {
                print '<div class="debug"><pre>'.$args_arr[$key].' = &gt;';

                print_r(htmlspecialchars($arg, ENT_COMPAT, 'UTF-8').'&lt;');
                
                print '</pre></div>'.PHP_EOL;
            }
            print '</div>'.PHP_EOL;
        }
        print '</div></pre>'.PHP_EOL;

    }

    // public function log($message)
    // {
    //     // $date = date("Y-m-d");
    //     // $file =$date.".log";
    //     // // var_dump($file);
    //     // if(!file_exists($file)){
    //     //     $log = fopen($file, 'a');
    //     // } else{
    //     //     $log = fopen($file, 'w');
    //     // }
    //     // fwrite($log , $message."\n");
    //     // fclose($log);
    //     $dir = __FILE__;
    //     $handle = fopen('./Storage/log/log.log', "r");
    //     // $homepage = file_get_contents('./../../Storage/log/log.log');
    //     echo $handle;
    // }

}