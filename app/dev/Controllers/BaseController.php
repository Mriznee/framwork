<?php

namespace Dev\Controllers;


class BaseController
{

    public function __construct()
    {
       
    }

    public function index()
    {
       print_r("base controller Index test");
    }

    public function create()
    {

    }

    public function edit()
    {

    }

    public function store()
    {

    }

    public function update()
    {

    }

    public function destroy()
    {
        
    }

    public function view($filename, $data )
    {
        $class = explode('\\',get_class($this));
        $viewlocation = ($class[2]);
        $viewfile =str_replace( array('Controller'), ' ', $viewlocation);    
        include('./../src/view/'.$viewfile.'/'.$filename.'.php');
    }

    

    
}