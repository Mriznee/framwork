<?php

namespace Dev\Http;

use Dev\Traits\Debug;


class Request
{
    use Debug;
    
    public $request =[];

    public function __construct()
    {
        $this->setRequest();
        $this->sendRequest();
        
    }

    private function getRequestMethod()
    {
        if(isset($_SERVER['REQUEST_METHOD']))
        {
            return explode('?',$_SERVER['REQUEST_METHOD']);
        }
        else 
        {
            $this->errorMessage('Could find the Request 102-REQUEST METHOD');
        }
    }

    private function getInput($requestType)
    {
        switch($requestType[0])
        {
            case 'GET':
                $request =[$_GET];
                break;
            case 'POST':      
                $request =[$_POST];
                break;
            case 'PUT':
                $request =[$_PUT];
                break;
            default:
                $this->errorMessage('Request Could not SET');
                break;
        } 
        if(isset($request))
        {
            return  $request;
        }
        else 
        {
            $this->errorMessage('Could not get the Data from the request 103 - Data not found');
        }    
    }

    private function setRequest()
    {
        $requestType = $this->getRequestMethod();
        $data = $this->getInput($requestType);   
        $this->request =
            [
                'request_type'  => $requestType,
                'data'          => $data
            ];
    }

    public function sendRequest()
    {
        return $this->request;
    }

  
}