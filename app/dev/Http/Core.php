<?php

namespace Dev\Http;

use Dev\Traits\Debug;
use Dev\Helpers\ErrorHandler;

class Core
{

    use Debug;


    public $url     = Array();
    public $route   = Array();
    public $config  = Array();
    public $request = Array();
       
    private function __constructor(){}

    public function  collector ($request, $route, $config,$url)
    {
       $this->request = $request;
       $this->route = $route;
       $this->config = $config;
       $this->url = $url;
       $this->goRoute();
    }
    
    private function goRoute()
    {         
    //    /*  */ $this->debug($this->app);
        if(isset($this->url))
        {
            $url_resourse = (explode("/", $this->url->url[0]));      
        }
        $this->routeCall(); 
    }

    private function routeCall($id = NULL)
    {
        if(isset($this->route['controller']))
        {
            $controllerAndMethod = (explode("@", $this->route['controller']));
        }
        
        if (!isset($controllerAndMethod[0]))
        {
            return ErrorHandler::errorMessage("Controller Not Found");
        }
        else
        {
            $controller = "\Learn\Controllers\\".$controllerAndMethod[0];
            $method = $controllerAndMethod[1];
        }

        if ( class_exists($controller))
        {
            if(isset($method))
            {
                if(method_exists($controller,$method)) {
                    $method = strtolower($method);
                    $app = new $controller();
                    $app->{$method}($id); 
                } else {
                    return ErrorHandler::errorMessage("Method not found ");
                }
            }
           
        }
        else
        {
       return ErrorHandler::errorMessage("Controller Not Found");
        }
    }
}