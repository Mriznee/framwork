<?php

namespace Dev\DB;

use Dev\Traits\Debug;
use Dev\Helpers\ErrorHandler;
use PDO;

class Db
{
    use Debug;


    public $db;
    private $config = Array();


    public function __construct($config)
    {
         $this->config = array (
            'serverType'    => $config['DB_CONNECTION'],
            'host'          => $config['DB_HOST'],
            'port'          => $config['DB_PORT'],
            'database'      => $config['DB_DATABASE'],
            'username'      => $config['DB_USERNAME'],
            'password'      => $config['DB_PASSWORD'],
         );
        $this->connect();
        $this->disconnect();
    }

    public function connect()
    {
        try{
            $this->db = new PDO('mysql:host='.$this->config['host'].';port='.$this->config['port'].';dbname='.$this->config['database'], $this->config['username'], $this->config['password']);
        }
        catch(mysql_error $e)
        {
            ErrorHandler::errorMessage("Error in connection".$e->getMessage());
        }
    }        

    public function disconnect()
    {
        $this->db = null;
    } 

    public function get($tablename, $prams, $feilds)
    {
        $sql = 'SELECT'. $feilds.'FROM'.$tablename.''.$prams;

    }

    public function create($tablename, $prams, $feilds)
    {
        $sql = 'INSERT INTO'. $feilds.'FROM'.$tablename.''.$prams;
    }

    public function update($tablename, $prams, $feilds)
    {
        $sql = 'UPDATE'. $feilds.'FROM'.$tablename.''.$prams;
    }

    public function delete($tablename, $prams, $feilds)
    {
        $sql = 'DELETE'. $feilds.'FROM'.$tablename.''.$prams;
    }




}