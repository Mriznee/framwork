<?php

namespace Learn\Core;

use Dev\Traits\Container;
use Dev\Traits\Debug;
use Dev\Helpers\ErrorHandler;
use Dev\System\Route as BaseRoute;


class Route extends BaseRoute
{
    use Debug;
    use Container;
    
    public $get     = Array();
    public $post    = Array();
    public $put     = Array();
    public $delete  = Array();
    public $url     = Array();
    public $method  = null;
    public $routeList   = Array();
    public $sendUrl     = Array();
    public $redirect    = Array();


    
    public function __construct()
    {
        $this->loadRoute();
        $this->getUrl();
        $this->getMethod();
    }

    public function loadRoute()
    {
        $this->routeList = require_once __DIR__ . ('/../Config/Route.php');       
    }


}