<?php

namespace Learn\Core;

use Dev\System\ServiceContainer;
use Dev\Traits\Debug;
use Dev\DB\DB;


class System
{
    use Debug;
    // use Container;
    
   

    public $app; 
    public $service = [];
    public $config = [];
    
    
    public function __construct()
    {
        $this->app = ServiceContainer::getInstance();
        $this->loadService();     
        $this->loadConfig(); 
        $this->loadDb();
        $this->updateRoute();
        $this->development();
        // $this->dump($this->app);
        // $this->debug($this->service);
        // phpinfo();
    }
    
    
    private function loadService()
    {   
        $serviceProvider = require_once __DIR__.('/../Config/ServiceProvider.php');
        foreach($serviceProvider as $key => $value)
        {
            $this->service = $this->app::singlton($key, $value);
        }
        
    }  
    
    private function loadConfig()
    {
        $this->config = require_once __DIR__ .('/../Config/Config.php');
    }

    public function loadDb()
    {
        $this->service = $this->app::singltonWith('DB', new Db($this->config), $this->config);
    }

    private function updateRoute()
    {  
        $request = $this->service['Request']->request; 
        $route   = $this->service['Route']->search_route( $this->service['Url']->url[0], $this->service['Route']->routeList); 
        $this->service['Core']->collector($request, $route, $this->config, $this->service['Url']);          
    }

    private function development()
    {
        
        $this->dump($this->service['Core']);
        $this->dump($this->service['Url']->url[0]);

    }

    
   

}