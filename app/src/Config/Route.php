<?php

return array (
    'default'  =>  array(
            'name' => 'index',
            'controller' => 'HomeController@index',
            'url' => '/',
            'request_type' => 'get',
            'params' =>null
           ),
          
    'home'  =>  array(
            'name' => 'index',
            'controller' => 'HomeController@index',
            'url' => '/home',
            'request_type' => 'get',
            'params' =>null
    ),

    'contacts'  =>  array(
            'name' => 'index',
            'controller' => 'AboutController@index',
            'url' => '/contactus',
            'request_type' => 'get',
            'params' =>null
    )
        
);



/*
 eppressions ,name , controller@method
/name/{variable}/name/{variable}?filter=?sort=?prams
Route::get      [Get Method ]
Route::pos      [POST Method ]
Route::put      [PUT Method ]
Route::delete   [Delete Method ]
Route::redirect('', 'URI', 301);
Route::any()
Route::resource [create routes for
                    Route::get [Get Method 
                    Route::pos [POST Method 
                    Route::put [PUT Method 
                    Route::delete [Delete Method];

 domainName/search?category=shoe&brand=nike&color=red&size=5.                  
*/

// Route::get("/{id}",'home','HomeController@index');
// Route::get("/home",'home','HomeController@index');
// Route::get("/contacts",'contacts','ContactsController@index');


// Route::get("/getroute/{id}/getroute2/{id2}",'home','HomeController@index');
// Route::post("/post/{id1}/post2/{id2}",'home','HomeController@index');
// Route::put("/put/{id1}/put2/{id2}",'home','HomeController@index');
// Route::resource("/resource1/{id1}/resource2/{id2}/resource3/{id3}",'home','HomeController@index');




